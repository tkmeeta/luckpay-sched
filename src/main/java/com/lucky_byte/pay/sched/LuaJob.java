/* Copyright (c) 2015,2016 Lucky Byte, Inc.
 */
package com.lucky_byte.pay.sched;

import java.io.File;
import java.util.List;

import org.luaj.vm2.LuaValue;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.lucky_byte.pay.jar.LuaInterp;
import com.lucky_byte.pay.jar.lualib.LuaFreeMarker;
import com.lucky_byte.pay.jar.lualib.LuaWebkitPDF;

/**
 * Lua 任务引擎
 */
public class LuaJob extends AbstractJob
{
	@Override
	public void doJob(File file, List<String> params,
			JobExecutionContext context) throws JobExecutionException {
		LuaInterp interp = new LuaInterp();

		// 报表处理模块
		interp.load(new LuaFreeMarker());
		interp.load(new LuaWebkitPDF());

		// 命令行参数
		LuaValue lua_args = LuaValue.tableOf();
		lua_args.set(-1, "Luckpay-LuaJ");
		lua_args.set(0, this.script);
		if (params != null) {
			for (int i = 0; i < params.size(); i++) {
				lua_args.set(i + 1, params.get(i));
			}
		}
		interp.set("arg", lua_args);
		interp.eval(file);
	}

}
