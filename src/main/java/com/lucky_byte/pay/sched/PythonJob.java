/* Copyright (c) 2015,2016 Lucky Byte, Inc.
 */
package com.lucky_byte.pay.sched;

/**
 * Python 脚本任务
 */
public class PythonJob extends SystemJob
{
	public PythonJob() {
		this.executable = this.lookForProgram("python");
	}
}
