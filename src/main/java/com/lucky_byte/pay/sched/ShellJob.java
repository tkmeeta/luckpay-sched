/* Copyright (c) 2015,2016 Lucky Byte, Inc.
 */
package com.lucky_byte.pay.sched;

/**
 * Shell 脚本任务
 */
public class ShellJob extends SystemJob
{
	public ShellJob() {
		this.executable = this.lookForProgram("sh");
	}
}
