/* Copyright (c) 2015,2016 Lucky Byte, Inc.
 */
package com.lucky_byte.pay.sched;

/**
 * Ruby 脚本任务
 */
public class RubyJob extends SystemJob
{
	public RubyJob() {
		this.executable = this.lookForProgram("ruby");
	}
}
