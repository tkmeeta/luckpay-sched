/* Copyright (c) 2015,2016 Lucky Byte, Inc.
 */
package com.lucky_byte.pay.sched;

/**
 * Perl 脚本任务
 */
public class PerlJob extends SystemJob
{
	public PerlJob() {
		this.executable = this.lookForProgram("perl");
	}
}
