package com.lucky_byte.pay.sched.test;

import java.util.Calendar;

import junit.framework.TestCase;

public class CalendarTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void test() {
		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.get(Calendar.YEAR));
		System.out.println(calendar.get(Calendar.YEAR) % 100);
		System.out.println(String.format("%02d", calendar.get(Calendar.MONTH)));
		System.out.println(calendar.get(Calendar.MINUTE));
	}
}
