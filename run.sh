#!/bin/sh
#
# Copyright (c) 2016 Lucky Byte, Inc.
#

runjar='luckpay_sched-1.0.1-shaded.jar'

pid=`ps xa | grep $runjar | grep java | awk -F ' ' '{print $1}'`

if test "x$1" = "xstatus" -o "x$1" = "x--status" ; then
    if test "x$pid" != "x" ; then
        echo "Sched 服务进程 $pid 正在运行 ..."
    else
        echo "Sched 服务进程已停止."
    fi
    exit 0
fi

if test "x$pid" != "x" ; then
    echo "Sched 服务进程 $pid 正在运行，发送中断信号并等待 3 秒 ..."
    kill $pid
    sleep 3
fi
if test "x$1" = "xstop" -o "x$1" = "x--stop" ; then
    exit 0
fi

RUNT_BASE=$PWD

nohup java -DRUNT.BASE=$RUNT_BASE -DRUNT.SECL=1 \
    -Dlog4j.configurationFile=$RUNT_BASE/log4j2.xml \
    -jar bin/$runjar &
